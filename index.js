// console.log("Hello World!");


// ALL TO DO LIST ITEMS
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => console.log(json));


// SINGLE TO DO LIST = GET METHOD
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => console.log(json));


// MAP METHOD*invalid*
// fetch('https://jsonplaceholder.typicode.com/todos/1')
// .then(response => map.json(title))
// .then((json) => console.log(title));



// PRINT MESSAGE = POST METHOD
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST", 
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Created a New to-do-list Item",
		completed: false,
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// UPDATE THE LIST = PUT METHOD
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		id: 1,
		title: "Updated to-do-lists",
		description: "Updated a to-do-list with a different data structure",
		status: "Pending",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// UPDATE STATUS to COMPLETE: PATCH METHOD
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		status: "Complete",
		dateCompleted: "2022-10-26"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// DELETE METHOD
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
});